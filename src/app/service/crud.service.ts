import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  result: any;
  constructor() { }

  resultData(data){
    this.result=data;
    console.log("RESULT:" + this.result);
  }
  getResult(){
    return this.result;
  }
}
